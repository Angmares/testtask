package com.agileengine.testtask.utils;

import org.jsoup.nodes.Element;

public class XmlUtils {
	public static String getElementTextRepresentation(Element element) {
		String representation = element.tagName();
		if (element.hasAttr("id")) {
			representation = representation + "." + element.attr("id");
		}

		return representation;
	}
	
	public static String getMainElementTextRepresentation(Element element) {
		if (element.hasAttr("id")) {
			return element.tagName() + "." + element.attr("id");
		}
		else
		{
			return element.toString();
		}
	}
}
