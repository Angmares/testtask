package com.agileengine.testtask.utils;

public class ArgumentValidator {
	public static int MIN_NUMBER_OF_ARGUMENTS = 2;
	public static int MAX_NUMBER_OF_ARGUMENTS = 3;
	
	public static void validateNumberOfArguments(String[] args)
	{
		if(args.length < MIN_NUMBER_OF_ARGUMENTS)
		{
			throw new IllegalArgumentException("At least " + MIN_NUMBER_OF_ARGUMENTS + " arguments required for aplication start");
		}
		if(args.length > MAX_NUMBER_OF_ARGUMENTS)
		{
			System.out.println("Applicatin requires maximum of " + MAX_NUMBER_OF_ARGUMENTS + " arguments. All other arguments will be ignored");
		}
	}
}
