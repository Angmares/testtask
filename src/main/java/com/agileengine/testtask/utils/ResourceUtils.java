package com.agileengine.testtask.utils;

import java.io.File;
import java.io.IOException;
import java.net.URL;

import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;

public class ResourceUtils {
	public static Document getDocumentFromResource(String resource) throws IOException {
		if(isValidURL(resource))
		{
			return Jsoup.connect(resource).get();
		}
		else
		{
			File htmlFile = new File(resource);
			if(htmlFile.exists())
			{
				return Jsoup.parse(htmlFile, null);
			}
			else
			{
				throw new IllegalArgumentException(resource + " is neither valid url or existing file");
			}
		}
	}

	public static boolean isValidURL(String resource) {
		try {
			new URL(resource).toURI();
			return true;
		} catch (Exception exception) {
			return false;
		}
	}
}
