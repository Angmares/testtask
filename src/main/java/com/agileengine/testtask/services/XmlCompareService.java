package com.agileengine.testtask.services;

import java.io.IOException;
import java.util.Collections;
import java.util.List;
import java.util.stream.Collectors;

import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;

import com.agileengine.testtask.grabbers.ElementGrabber;
import com.agileengine.testtask.grabbers.SameIdGrabber;
import com.agileengine.testtask.utils.ResourceUtils;
import com.agileengine.testtask.utils.XmlUtils;

public class XmlCompareService {
	private String originalFile;
	private String targetFile;
	private ElementGrabber grabber;

	public XmlCompareService(String originalFile, String targetFile) {
		this.originalFile = originalFile;
		this.targetFile = targetFile;
		this.grabber = new SameIdGrabber();
	}

	private Document getDocument(String pathToFile) throws IOException {
		return ResourceUtils.getDocumentFromResource(pathToFile);
	}

	private Element getSearchedElement(String id) throws IOException {
		Document originalDocument = getDocument(originalFile);
		Element element = originalDocument.getElementById(id);
		if(element == null)
		{
			throw new IllegalArgumentException("No e;ement with " + id + " in original document");
		}
		return element;
	}

	private Element getSimilarElement(String originalElementId) throws IOException {
		Element original = getSearchedElement(originalElementId);
		Document targetDocument = getDocument(targetFile);

		List<Element> elements = grabber.getSimilarElement(original, targetDocument);
		if (elements != null && elements.size() != 0) {
			if (elements.size() == 1) {
				System.out.println("Closest match foud: ");
			} else {
				System.out.println("Several similar elements found. Displaying the most likely candidate: ");
			}

			return elements.get(0);
		}

		return null;
	}

	public String getSimilarElementPath(String originalElementId) throws IOException {
		Element element = getSimilarElement(originalElementId);
		if (element != null) {
			List<Element> parentElements = element.parents().clone();
			Collections.reverse(parentElements);
			String parents = parentElements.stream()
					.map(parentElement -> XmlUtils.getElementTextRepresentation(parentElement))
					.collect(Collectors.joining(" > "));
			return parents + " > " + XmlUtils.getMainElementTextRepresentation(element);
		} else {
			return "Stop playing tricks with me, those two documents look completly different";
		}
	}
}
