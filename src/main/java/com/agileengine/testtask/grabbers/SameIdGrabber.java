package com.agileengine.testtask.grabbers;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;

public class SameIdGrabber extends ElementGrabber {
	public SameIdGrabber()
	{
		super(new SameTextGrabber());
	}

	@Override
	protected List<Element> tryElementSearch(Element originalElement, Document targetDocument) {
		System.out.println("Looking for element with the same id...");
		Element element = targetDocument.getElementById(originalElement.attr("id"));
		ArrayList<Element> elements = new ArrayList<Element>();
		if (element != null)
		{
			elements.add(element); 
		}
		return elements;
	}

	@Override
	protected List<Element> tryElementSearch(Element originalElement, List<Element> elements) {
		System.out.println("Looking for element with the same id...");
		return elements.stream()
				.filter(element -> originalElement.attr("id").equals(element.attr("id"))).collect(Collectors.toList());
	}
}
