package com.agileengine.testtask.grabbers;

import java.util.Arrays;
import java.util.List;
import java.util.stream.Collectors;

import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;

public class SimilarTextGrabber extends ElementGrabber {
	public SimilarTextGrabber()
	{
		super(new SimilarAttributeGrabber());
	}

	@Override
	protected List<Element> tryElementSearch(Element originalElement, Document targetDocument) {
		System.out.println("Getting elements by tag...");
		List<Element> possibleElements = targetDocument.getElementsByTag(originalElement.tagName());
		if(possibleElements != null && possibleElements.size() > 1)
		{
			return this.tryElementSearch(originalElement, possibleElements);
		}

		return possibleElements;
	}

	@Override
	protected List<Element> tryElementSearch(Element originalElement, List<Element> elements) {
		System.out.println("Looking for similar text...");
		String originalText = originalElement.text();

		List<Element> filtered = elements.stream().filter(element -> compareText(originalText, element.text())).collect(Collectors.toList());

		return filtered;
	}

	private boolean compareText(String originalText, String targetText)
	{
		List<String> targetWords = Arrays.asList(targetText.split(" ")).stream().map(word -> word.toLowerCase()).collect(Collectors.toList());
		List<String> originalWords = Arrays.asList(originalText.split(" ")).stream().map(word -> word.toLowerCase()).collect(Collectors.toList());
		if(originalWords.containsAll(targetWords))
		{
			System.out.println("Element similar text found.");
			return true;
		}
		
		if(targetWords.containsAll(originalWords))
		{
			System.out.println("Element similar text found");
			return true;
		}
		
		return false;
	}
}
