package com.agileengine.testtask.grabbers;

import java.util.List;
import java.util.stream.Collectors;

import org.jsoup.nodes.Attribute;
import org.jsoup.nodes.Attributes;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;

import com.agileengine.testtask.utils.XmlUtils;

public class SimilarAttributeGrabber extends ElementGrabber {
	public SimilarAttributeGrabber() {
		super(null);
	}

	@Override
	protected List<Element> tryElementSearch(Element originalElement, Document targetDocument) {
		System.out.println("Comparing element attributes...");
		List<Element> possibleElements = targetDocument.getElementsByTag(originalElement.tagName());
		if (possibleElements != null && possibleElements.size() != 0) {
			return this.tryElementSearch(originalElement, possibleElements);
		}

		return possibleElements;
	}

	@Override
	protected List<Element> tryElementSearch(Element originalElement, List<Element> elements) {
		System.out.println("Comparing element attributes...");
		return elements.stream().sorted((element1, element2) -> compareAttributes(originalElement, element2, element1))
				.collect(Collectors.toList());
	}

	private Integer compareAttributes(Element originalElement, Element targetElement1, Element targetElement2) {
		Integer matchingAttributes1 = getNumberOfMatchingAttributes(originalElement, targetElement1);
		Integer matchingAttributes2 = getNumberOfMatchingAttributes(originalElement, targetElement2);
		return matchingAttributes1 - matchingAttributes2;

	}

	private Integer getNumberOfMatchingAttributes(Element originalElement, Element targetElement) {
		Integer matchingAttributes = 0;
		Attributes originalAttributes = originalElement.attributes();
		for (Attribute attribute : originalAttributes.asList()) {
			if (targetElement.hasAttr(attribute.getKey())) {
				System.out.println("Element " + XmlUtils.getElementTextRepresentation(targetElement)
						+ " has same atribute " + attribute.getKey());
				matchingAttributes++;
				if (targetElement.attr(attribute.getKey()).equalsIgnoreCase(attribute.getValue())) {
					System.out.println("Element " + XmlUtils.getElementTextRepresentation(targetElement) + " has same "
							+ attribute.getKey() + " value " + targetElement.attr(attribute.getKey()));
					matchingAttributes++;
				}
			}
		}
		System.out.println("Element " + XmlUtils.getElementTextRepresentation(targetElement) + " has "
				+ matchingAttributes + " similarities");
		return matchingAttributes;
	}
}
