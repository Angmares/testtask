package com.agileengine.testtask.grabbers;

import java.util.List;

import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;

public abstract class ElementGrabber {
	private ElementGrabber grabber;

	public ElementGrabber(ElementGrabber grabber) {
		this.grabber = grabber;
	}

	public List<Element> getSimilarElement(Element originalElement, Document targetDocument) {
		List<Element> elements = this.tryElementSearch(originalElement, targetDocument);
		if (grabber != null) {
			if (elements == null || elements.isEmpty()) {
				System.out.println("No elements found, using next grabber");
				return grabber.getSimilarElement(originalElement, targetDocument);
			}
			if (elements.size() > 1) {
				System.out.println("Too many elements found, using next grabber to filter");
				return grabber.getSimilarElement(originalElement, elements);
			}
		}

		if(elements == null || elements.size() != 1)
		{
			System.out.println("No more grabbers in chain.");
		}

		return elements;
	}

	protected List<Element> getSimilarElement(Element originalElement, List<Element> passedElements) {
		List<Element> elements = this.tryElementSearch(originalElement, passedElements);
		if (elements.size() > 1 && grabber != null) {
			return grabber.getSimilarElement(originalElement, elements);
		}

		return elements;
	}

	protected abstract List<Element> tryElementSearch(Element originalElement, Document targetDocument);

	protected abstract List<Element> tryElementSearch(Element originalElement, List<Element> elements);
}
