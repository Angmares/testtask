package com.agileengine.testtask.grabbers;

import java.util.List;
import java.util.stream.Collectors;

import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;

public class SameTextGrabber extends ElementGrabber {
	public SameTextGrabber()
	{
		super(new SimilarTextGrabber());
	}

	@Override
	protected List<Element> tryElementSearch(Element originalElement, Document targetDocument) {
		System.out.println("Getting elements by tag...");
		List<Element> possibleElements = targetDocument.getElementsByTag(originalElement.tagName());
		if(possibleElements != null && possibleElements.size() > 1)
		{
			return this.tryElementSearch(originalElement, possibleElements);
		}

		return possibleElements;
	}

	@Override
	protected List<Element> tryElementSearch(Element originalElement, List<Element> elements) {
		System.out.println("Looking for same text...");
		String originalText = originalElement.text();

		List<Element> filtered = elements.stream().filter(element -> compareText(originalText, element.text())).collect(Collectors.toList());

		return filtered;
	}

	private boolean compareText(String originalText, String targetText)
	{
		if(originalText.trim().equalsIgnoreCase(targetText.trim()))
		{
			System.out.println("Element with same text found.");
			return true;
		}
		return false;
	}
}
