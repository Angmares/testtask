package com.agileengine.testtask;

import java.io.IOException;

import com.agileengine.testtask.services.XmlCompareService;
import com.agileengine.testtask.utils.ArgumentValidator;

public class Main {

	public static void main(String[] args) {
		ArgumentValidator.validateNumberOfArguments(args);

		String id = "make-everything-ok-button";
		String originalFile = args[0];
		String targetFile = args[1];
		if(args.length == 3)
		{
			id = args[2];
		}

		XmlCompareService comparator = new XmlCompareService(originalFile, targetFile);
		try {
			System.out.println(comparator.getSimilarElementPath(id));
		} catch (IOException e) {
			System.out.println("Something went wrong while reading files.");
			System.out.println("Please check if files have all required permissions and are not locked with other processes");
		}
	}

}
